module Lib.manag {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens Lib.manag to javafx.fxml;
    exports Lib.manag;
}