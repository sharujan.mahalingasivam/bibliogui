package Lib.manag;

import javafx.beans.property.SimpleStringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class OeuvreEmprunt extends Oeuvre {
    OeuvreEmprunt(Connect conn, ResultSet resultatInstance) throws SQLException {
        super(conn, resultatInstance);
    }
    OeuvreEmprunt(Connect conn, ArrayList<SimpleStringProperty> instanceACreer){
        super(conn, instanceACreer);
        this.nomColonnes.addAll(Arrays.asList("id_emprunt","id_oeuvre", "titre", "genre","id_auteur"));
    }

    @Override
    public String toString() {
        return ""+instanceTotale.get(2).get();
    }
}
