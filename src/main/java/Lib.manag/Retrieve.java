package Lib.manag;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Retrieve {
    String id_user = "0";
    Connect conn;
    Retrieve(Connect conn) {
        this.conn = conn;

    }
    public  ArrayList<Auteur> RetrieveAuteur() {
        Statement statement = null;
        ArrayList<Auteur> auteurs = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM Auteur");
            auteurs = new ArrayList<Auteur>();

            while (resultat.next()) {
                Auteur a = new Auteur(conn, resultat);
                auteurs.add(a);
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        return auteurs;
    }
    public  ArrayList<Oeuvre> RetrieveOeuvre() {
        Statement statement = null;
        ArrayList<Oeuvre> oeuvres = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM Oeuvre");
            oeuvres = new ArrayList<Oeuvre>();

            while (resultat.next()) {
                Oeuvre a = new Oeuvre(conn, resultat);
                oeuvres.add(a);
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        return oeuvres;
    }
    public  ArrayList<Utilisateur> RetrieveUtilisateur() {
        Statement statement = null;
    ArrayList<Utilisateur> users = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM Utilisateur");
            users = new ArrayList<Utilisateur>();

            while (resultat.next()) {
                Utilisateur a = new Utilisateur(conn, resultat);
                users.add(a);
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        return users;
    }

    public ArrayList<Blacklist> RetrieveBlacklist(){
        Statement statement = null;
        ArrayList<Blacklist> blacklists = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM Blacklist");
            blacklists = new ArrayList<Blacklist>();

            while (resultat.next()) {
                Blacklist a = new Blacklist(conn, resultat);
                blacklists.add(a);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return blacklists;
    }

    public  ArrayList<OeuvreEmprunt> RetrieveOeuvreEmprunte(String id_user) {
        Statement statement = null;
        ArrayList<OeuvreEmprunt> oeuvres = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT id_emprunt,Oeuvre.id_oeuvre,titre,genre,id_auteur FROM Oeuvre JOIN Emprunt ON Oeuvre.id_oeuvre = Emprunt.id_oeuvre WHERE id_usager = "+ id_user);
            oeuvres = new ArrayList<OeuvreEmprunt>();

            while (resultat.next()) {
                OeuvreEmprunt a = new OeuvreEmprunt(conn, resultat);
                oeuvres.add(a);
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        return oeuvres;
    }

    public ArrayList<Oeuvre> RetrieveOeuvreEmprunteavecAuteur(String id_user) {
        Statement statement = null;
        ArrayList<Oeuvre> oeuvres = null;
        try {
            statement = conn.con.createStatement();
            ResultSet resultat = statement.executeQuery("SELECT Oeuvre.id_oeuvre,titre,genre,Oeuvre.id_auteur, prénom, nom, id_usager FROM Oeuvre JOIN Emprunt ON Oeuvre.id_oeuvre=Emprunt.id_oeuvre JOIN Auteur ON Oeuvre.id_auteur = Auteur.id_auteur WHERE id_usager =  "+ id_user);
            oeuvres = new ArrayList<Oeuvre>();
            while (resultat.next()) {
                Oeuvre a = new Oeuvre(conn, resultat);
                oeuvres.add(a);
            }
        }catch (Exception e) {
            System.out.println(e);
        }

        return oeuvres;
    }

    public ArrayList<Oeuvre> RetrieveOeuvreEmpruntable() {
        Statement statement = null;
        ArrayList<Oeuvre> oeuvresempruntes = null;
        try {
            statement = conn.con.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT Emprunt.id_oeuvre, titre,genre,id_auteur FROM Oeuvre JOIN Emprunt ON Oeuvre.id_oeuvre = Emprunt.id_oeuvre");
            oeuvresempruntes = new ArrayList<Oeuvre>();
            while (resultat.next()) {
                Oeuvre a = new Oeuvre(conn, resultat);
                oeuvresempruntes.add(a);
            }
        }catch (Exception e) {
            System.out.println(e);
        }

        ArrayList<Oeuvre> oeuvrestotal = this.RetrieveOeuvre();
        System.out.println(oeuvrestotal.size());

        ArrayList<Oeuvre> oeuvresempruntable = new ArrayList<>();

        for (int i = 0; i< oeuvrestotal.size();i++) {
            boolean estemp = true;
            for (int j = 0; j< oeuvresempruntes.size();j++){
                if(oeuvresempruntes.get(j).instanceTotale.get(0).get().equals(oeuvrestotal.get(i).instanceTotale.get(0).get())) {
                    estemp = false;
                }
            }
            if (estemp) {
                oeuvresempruntable.add(oeuvrestotal.get(i));
            }
        }
        return oeuvresempruntable;

    }
}




