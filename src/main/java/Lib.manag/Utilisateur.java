package Lib.manag;

import javafx.beans.property.SimpleStringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Utilisateur extends Table{
    Utilisateur(Connect conn, ResultSet resultatInstance) throws SQLException {
        super(conn, resultatInstance);
    }
    Utilisateur(Connect conn, ArrayList<SimpleStringProperty> instanceACreer){
        super(conn, instanceACreer);
        this.nomColonnes.addAll(Arrays.asList("id_usager", "prenom","nom","mail"));
    }

    public String getId_usager(){
        return instanceTotale.get(0).get();
    }
    public void setId_auteur(String id_usager){
        SimpleStringProperty tempObj = instanceTotale.get(0);
        tempObj.set(id_usager);
        this.instanceTotale.set(0, tempObj);
    }
    public String getPrenom(){
        return instanceTotale.get(1).get();
    }
    public void setPrenom(String prenom){
        SimpleStringProperty tempObj = instanceTotale.get(1);
        tempObj.set(prenom);
        this.instanceTotale.set(1, tempObj);
    }
    public String getNom(){
        return instanceTotale.get(2).get();
    }
    public void setNom(String nom){
        SimpleStringProperty tempObj = instanceTotale.get(2);
        tempObj.set(nom);
        this.instanceTotale.set(2,tempObj);
    }
    public String getMail(){
        return instanceTotale.get(3).get();
    }
    public void setmail(String id_categorie){
        SimpleStringProperty tempObj = instanceTotale.get(3);
        tempObj.set(id_categorie);
        this.instanceTotale.set(3, tempObj);
    }
    public String getId_categorie(){
        return instanceTotale.get(4).get();
    }
    public void setId_categorie(String id_categorie){
        SimpleStringProperty tempObj = instanceTotale.get(4);
        tempObj.set(id_categorie);
        this.instanceTotale.set(4,tempObj);
    }

    @Override
    public String toString() {
        return ""+instanceTotale.get(1).get() + " "+ instanceTotale.get(2).get();
    }
}
