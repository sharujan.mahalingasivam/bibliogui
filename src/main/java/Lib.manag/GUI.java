package Lib.manag;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.MenuBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.util.Duration;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class GUI extends Application {
    private Controller controller;
    private Menu Auteurs, Livres;
    private Retrieve retrieve;

        public static int pageAuteur(Stage stage, Retrieve retrieve, MenuBar menubar) {
            ArrayList<Auteur> auteurs = retrieve.RetrieveAuteur();
            ObservableList<Auteur> auteursObs = FXCollections.observableArrayList(auteurs);
            VBox rootpaut = new VBox();
            TableView<Auteur> auteursTables = new TableView<>();

            TableColumn<Auteur, String> AutPrenColumn = new TableColumn<>("Prenom");
            AutPrenColumn.setCellValueFactory(new PropertyValueFactory<>("prenom"));

            TableColumn<Auteur, String> AutNomColumn = new TableColumn<>("Nom");
            AutNomColumn.setCellValueFactory(new PropertyValueFactory<>("nom"));

            TableColumn<Auteur, String> AutDateColumn = new TableColumn<>("Date de Naissance");
            AutDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

            auteursTables.getColumns().addAll(AutPrenColumn, AutNomColumn, AutDateColumn);
            auteursTables.setItems(auteursObs);
            rootpaut.getChildren().clear();

            rootpaut.getChildren().addAll(menubar, auteursTables);
            Scene scene = new Scene(rootpaut, 600, 400);
            stage.setTitle("App d'essai");
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageOeuvre(Stage stage, Retrieve retrieve, MenuBar menubar) {

            ArrayList<Oeuvre> oeuvres = retrieve.RetrieveOeuvre();
            Jointer oeuvresprepar = new Jointer(oeuvres);
            ArrayList<Oeuvre> oeuvresetaut = oeuvresprepar.tableavecauteursdisplay();
            ObservableList<Oeuvre> oeuvresObs = FXCollections.observableArrayList(oeuvresetaut);
            VBox rootpoeuv = new VBox();
            TableView<Oeuvre> oeuvresTables = new TableView<>();

            TableColumn<Oeuvre, String> OeuvTitreColumn = new TableColumn<>("Titre");
            OeuvTitreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));

            TableColumn<Oeuvre, String> OeuvGenreColumn = new TableColumn<>("Genre");
            OeuvGenreColumn.setCellValueFactory(new PropertyValueFactory<>("genre"));

            TableColumn<Oeuvre, String> OeuvNomAutCol = new TableColumn<>("Auteur");
            OeuvNomAutCol.setCellValueFactory(new PropertyValueFactory<>("nomauteur"));

            oeuvresTables.getColumns().addAll(OeuvTitreColumn,OeuvNomAutCol,OeuvGenreColumn);
            oeuvresTables.setItems(oeuvresObs);
            rootpoeuv.getChildren().addAll(menubar,oeuvresTables);
            Scene scene = new Scene(rootpoeuv,600,400);
            stage.setTitle("app essai");
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pagelogiciel(Stage stage) {
            stage.centerOnScreen();
            VBox rootLogiciel = new VBox();
            ImageView imageView = new ImageView();
            imageView.setImage(new Image("file:src/main/resources/booksyle.gif"));
            rootLogiciel.getChildren().add(imageView);
            Scene scene = new Scene(rootLogiciel);
            stage.setScene(scene);
            stage.centerOnScreen();

            return 0;
        }
        public static int pagelogin(Stage stage, Connect con, Retrieve retrieve, MenuBar menubar){
            Controller controller = new Controller(con);
            Jointer jointer = new Jointer(retrieve.RetrieveOeuvre());
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Scene scene = new Scene(grid, 400, 400);

            Text scenetitre = new Text("Connexion");
            scenetitre.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            grid.add(scenetitre, 0, 0, 2, 1);

            Label userName = new Label("Pseudo:");
            grid.add(userName, 0, 1);

            TextField userTextField = new TextField();
            grid.add(userTextField, 1, 1);

            Label pw = new Label("Mot de passe:");
            grid.add(pw, 0, 2);

            PasswordField pwBox = new PasswordField();
            grid.add(pwBox, 1, 2);
            Button btn = new Button("Se connecter");
            HBox hbBtn = new HBox(10);
            hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
            hbBtn.getChildren().add(btn);
            grid.add(hbBtn, 1, 4);
            final Text correction = new Text();
            grid.add(correction, 1, 6);

            ToggleGroup group = new ToggleGroup();
            RadioButton admin = new RadioButton("Gestionnaire");
            admin.setSelected(true);

            admin.setToggleGroup(group);
            RadioButton user = new RadioButton("Utilisateur");
            user.setToggleGroup(group);
            HBox radios = new HBox();
            radios.getChildren().addAll(user, admin);
            grid.add(radios, 1, 3);

            btn.setOnAction(e -> {
                int validite = controller.loginVerif(userTextField.getText(),pwBox.getText(),((RadioButton)group.getSelectedToggle()).getText());
                retrieve.id_user = ""+validite;
                Menu Utilisateurs = new Menu();

                ArrayList<SimpleStringProperty> instancefictive = new ArrayList<>();
                instancefictive.add(new SimpleStringProperty(retrieve.id_user));
                Utilisateur user_fictif_test_ban = new Utilisateur(retrieve.conn, instancefictive);
                if (jointer.isbanned(user_fictif_test_ban)){
                    correction.setFill(Color.FIREBRICK);
                    correction.setText("Vous êtes banni !");
                }
                else{
                if (validite == 0){
                    correction.setFill(Color.GREEN);
                    correction.setText("Connexion réussie");
                    if (menubar.getMenus().size()==4) {
                        Utilisateurs = new Menu("Utilisateurs");
                        MenuItem usersAffiche = new MenuItem("Afficher");
                        MenuItem usersGere = new MenuItem("Gérer");
                        Utilisateurs.getItems().addAll(usersAffiche, usersGere);

                        menubar.getMenus().add(3, Utilisateurs);
                        MenuItem auteursGere = new MenuItem("Gérer");
                        menubar.getMenus().get(0).getItems().addAll(auteursGere);

                        MenuItem livresGere = new MenuItem("Gérer");
                        menubar.getMenus().get(1).getItems().addAll(livresGere);

                        usersAffiche.setOnAction(m -> {
                            int i = pageUtilisateur(stage, retrieve, menubar);
                        });
                        usersGere.setOnAction((a -> {
                            pageUtilisateurGestion(stage, retrieve, menubar);
                        }));
                        auteursGere.setOnAction(p -> {
                            pageAuteurGestion(stage, retrieve, menubar);
                        });


                        livresGere.setOnAction(n -> {
                            pageLivresGestion(stage, retrieve, menubar);
                        });
                    }
                    Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), l ->{pageAuteur(stage,retrieve,menubar);}));
                    timeline.play();

                }
                else if (validite == 1 || validite == 2){
                    correction.setFill(Color.GREEN);
                    correction.setText("Connexion réussie");
                    if(menubar.getMenus().size()>4){
                        menubar.getMenus().remove(3);
                        menubar.getMenus().get(0).getItems().remove(1);
                        menubar.getMenus().get(1).getItems().remove(1);
                    }
                    Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), l ->{pageAuteur(stage,retrieve,menubar);}));
                    timeline.play();
                }

                else {
                    correction.setFill(Color.FIREBRICK);
                    correction.setText("Pseudo et/ou MDP incorrect \n Veuillez réssayer");
                }}
            });

            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageUtilisateur(Stage stage, Retrieve retrieve, MenuBar menubar){
            ArrayList<Utilisateur> users = retrieve.RetrieveUtilisateur();
            ObservableList<Utilisateur> usersObs = FXCollections.observableArrayList(users);
            VBox rootpuser = new VBox();
            TableView<Utilisateur> usersTables = new TableView<>();

            TableColumn<Utilisateur, String> UserPrenColumn = new TableColumn<>("Prenom");
            UserPrenColumn.setCellValueFactory(new PropertyValueFactory<>("prenom"));


            TableColumn<Utilisateur, String> UserNomColumn = new TableColumn<>("Nom");
            UserNomColumn.setCellValueFactory(new PropertyValueFactory<>("nom"));


            TableColumn<Utilisateur, String> UserMailColumn = new TableColumn<>("Adresse mail");
            UserMailColumn.setCellValueFactory(new PropertyValueFactory<>("mail"));


            usersTables.getColumns().addAll(UserPrenColumn,UserNomColumn,UserMailColumn);
            usersTables.setItems(usersObs);
            usersTables.setEditable(true);
            rootpuser.getChildren().clear();

            rootpuser.getChildren().addAll(menubar, usersTables);
            Scene scene = new Scene(rootpuser, 600, 400);
            stage.setTitle("App d'essai");
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageUtilisateurGestion(Stage stage, Retrieve retrieve, MenuBar menubar){
            GridPane grid = new GridPane();
            grid.setHgap(5);
            grid.setVgap(5);
            grid.setPadding(new Insets(25, 25, 25, 25));

            RadioButton ajouter = new RadioButton("Ajouter");
            RadioButton modifier = new RadioButton("Modifier");
            RadioButton supprimer = new RadioButton("Supprimer");

            ToggleGroup group = new ToggleGroup();
            ajouter.setToggleGroup(group);
            modifier.setToggleGroup(group);
            supprimer.setToggleGroup(group);

            Label hautpage = new Label("Gestion des Utilisateurs");
            hautpage.setFont(Font.font("Tahoma",FontWeight.BOLD,20));
            grid.add(hautpage, 10,0,10,10);
            grid.add(ajouter,0,30);
            grid.add(modifier,10,30);
            grid.add(supprimer,20,30);

            Label labprenUserAjout = new Label("Prénom : ");
            TextField prenUserAjout = new TextField();
            Label labnomUserAjout = new Label("Nom : ");
            TextField nomUserAjout = new TextField();
            Label labmailUserAjout = new Label("Adresse mail : ");
            TextField mailUserAjout = new TextField();




            grid.add(labprenUserAjout,0,36);
            grid.add(prenUserAjout,1,36);
            grid.add(labnomUserAjout,0,37);
            grid.add(nomUserAjout,1,37);
            grid.add(labmailUserAjout,0,38);
            grid.add(mailUserAjout,1,38);




            Label labprenUserMod = new Label("Prénom : ");
            TextField prenUserMod = new TextField();
            Label labnomUserMod = new Label("Nom : ");
            TextField nomUserMod = new TextField();
            Label labmailUserMod = new Label("Adresse mail : ");
            TextField mailUserMod = new TextField();


            grid.add(labprenUserMod, 10,36);
            grid.add(prenUserMod,11,36);
            grid.add(labnomUserMod,10,37);
            grid.add(nomUserMod,11,37);
            grid.add(labmailUserMod,10,38);
            grid.add(mailUserMod,11,38);



            prenUserAjout.setDisable(true);
            nomUserAjout.setDisable(true);
            mailUserAjout.setDisable(true);

            prenUserMod.setDisable(true);
            nomUserMod.setDisable(true);
            mailUserMod.setDisable(true);





            ComboBox chk = new ComboBox();
            ArrayList<Utilisateur> users = retrieve.RetrieveUtilisateur();
            ObservableList<Utilisateur> usersObs = FXCollections.observableArrayList(users);
            ArrayList<Utilisateur> users2 = retrieve.RetrieveUtilisateur();
            ObservableList<Utilisateur> usersObs2 = FXCollections.observableArrayList(users);

            ComboBox chkSup = new ComboBox();
            chkSup.setItems(usersObs2);

            Label labSup = new Label("Utilisateur");
            grid.add(labSup,20,36);
            grid.add(chkSup,21,36);
            Jointer jointer = new Jointer(retrieve.RetrieveOeuvre());

            Label labUseramodif = new Label("Utilisateur : ");
            grid.add(labUseramodif,10,35);
            grid.add(chk, 11,35);
            chk.setDisable(true);
            ToggleGroup group2 = new ToggleGroup();
            RadioButton ban = new RadioButton("Banni");
            RadioButton noban = new RadioButton("Non banni");
            ban.setToggleGroup(group2);
            noban.setToggleGroup(group2);
            Label labstat = new Label("Statut : ");

            grid.add(labstat, 10,39);
            grid.add(noban,11,39);
            grid.add(ban,11,40);

            chk.setItems(usersObs);
            chk.setOnAction(e ->{
                prenUserMod.setText(((Utilisateur)chk.getValue()).instanceTotale.get(1).get());
                nomUserMod.setText(((Utilisateur)chk.getValue()).instanceTotale.get(2).get());
                mailUserMod.setText(((Utilisateur)chk.getValue()).instanceTotale.get(3).get());

                if(jointer.isbanned(((Utilisateur)chk.getValue()))) {
                    ban.setSelected(true);
                }
                else {
                    noban.setSelected(true);
                }
            });


            prenUserAjout.setDisable(true);
            nomUserAjout.setDisable(true);
            mailUserAjout.setDisable(true);

            prenUserMod.setDisable(true);
            nomUserMod.setDisable(true);
            mailUserMod.setDisable(true);
            chk.setDisable(true);
            ban.setDisable(true);
            noban.setDisable(true);

            chkSup.setDisable(true);

            group.selectedToggleProperty().addListener((observable, oldToggle,newToggle) ->{
                if (((RadioButton)newToggle).getText() == "Ajouter") {
                    prenUserAjout.setDisable(false);
                    nomUserAjout.setDisable(false);
                    mailUserAjout.setDisable(false);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    mailUserMod.setDisable(true);
                    chk.setDisable(true);
                    ban.setDisable(true);
                    noban.setDisable(true);

                    chkSup.setDisable(true);
                }
                else if (((RadioButton)newToggle).getText() == "Modifier") {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    mailUserAjout.setDisable(true);

                    prenUserMod.setDisable(false);
                    nomUserMod.setDisable(false);
                    mailUserMod.setDisable(false);
                    chk.setDisable(false);
                    ban.setDisable(false);
                    noban.setDisable(false);

                    chkSup.setDisable(true);
                }
                else {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    mailUserAjout.setDisable(true);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    mailUserMod.setDisable(true);
                    chk.setDisable(true);
                    ban.setDisable(true);
                    noban.setDisable(true);

                    chkSup.setDisable(false);
                }

            });

            Button validation = new Button("Valider");
            Text trigger = new Text();
            grid.add(trigger,11, 41);
            grid.add(validation, 11, 45);

            validation.setOnAction(m -> {
                if (((RadioButton)group.getSelectedToggle()).getText() == "Ajouter"){

                    trigger.setFill(Color.GREEN);
                    trigger.setText("Utilisateur ajouté");

                    ArrayList<SimpleStringProperty> newInstance = new ArrayList<>();
                    newInstance.add(new SimpleStringProperty(prenUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(nomUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(mailUserAjout.getText()));

                    Utilisateur newuser = new Utilisateur(retrieve.conn, newInstance);
                    try {
                        newuser.Insertdb();
                    }
                    catch(SQLException e) {
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Modifier"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Utilisateur modifié");

                    ArrayList<Object> newdata = new ArrayList<>();
                    newdata.add(((Utilisateur)chk.getValue()).instanceTotale.get(0).get());
                    newdata.add(prenUserMod.getText());
                    newdata.add(nomUserMod.getText());
                    newdata.add(mailUserMod.getText());
                    if (((RadioButton)group2.getSelectedToggle()).getText() == "Non banni" && jointer.isbanned(((Utilisateur)chk.getValue()))){
                        try {
                            Statement stat = retrieve.conn.con.createStatement();
                            stat.executeUpdate("DELETE FROM Blacklist WHERE id_usager = "+ ((Utilisateur)chk.getValue()).instanceTotale.get(0).get());
                            stat.close();
                            retrieve.conn.con.commit();
                        }
                        catch(SQLException e) {
                            System.out.println(e);
                        }
                    }
                    else if (((RadioButton)group2.getSelectedToggle()).getText() == "Banni" && !jointer.isbanned(((Utilisateur)chk.getValue()))){
                        ArrayList<SimpleStringProperty> newinst = new ArrayList<>();
                        newinst.add(((Utilisateur)chk.getValue()).instanceTotale.get(0));
                        Blacklist newbl = new Blacklist(retrieve.conn, newinst);
                        try {
                            newbl.Insertdb();
                        }
                        catch(SQLException e) {
                            System.out.println(e);
                        }
                    }
                    try {
                    ((Utilisateur)chk.getValue()).Updatedb(newdata);
                    }
                    catch(SQLException e){
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Supprimer"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Utilisateur supprimé");
                    try {

                        ((Utilisateur) chkSup.getValue()).Deletedb();
                    }
                    catch(SQLException e) {

                    }
                }
                else {
                    trigger.setFill(Color.FIREBRICK);
                    trigger.setText("Veuillez choisir une modification à effectuer");
                }
            });
            VBox rootpagegestionUser = new VBox();
            rootpagegestionUser.getChildren().addAll(menubar,grid);
            Scene scene = new Scene(rootpagegestionUser, 1000,480);
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageAuteurGestion(Stage stage, Retrieve retrieve, MenuBar menubar){
            GridPane grid = new GridPane();
            grid.setHgap(5);
            grid.setVgap(5);
            grid.setPadding(new Insets(25, 25, 25, 25));

            RadioButton ajouter = new RadioButton("Ajouter");
            RadioButton modifier = new RadioButton("Modifier");
            RadioButton supprimer = new RadioButton("Supprimer");

            ToggleGroup group = new ToggleGroup();
            ajouter.setToggleGroup(group);
            modifier.setToggleGroup(group);
            supprimer.setToggleGroup(group);

            Label hautpage = new Label("Gestion des Auteurs");
            hautpage.setFont(Font.font("Tahoma",FontWeight.BOLD,20));
            grid.add(hautpage, 10,0,10,10);
            grid.add(ajouter,0,30);
            grid.add(modifier,10,30);
            grid.add(supprimer,20,30);

            Label labprenUserAjout = new Label("Prénom : ");
            TextField prenUserAjout = new TextField();
            Label labnomUserAjout = new Label("Nom : ");
            TextField nomUserAjout = new TextField();
            Label labmailUserAjout = new Label("Année de naissance : ");
            TextField mailUserAjout = new TextField();




            grid.add(labprenUserAjout,0,36);
            grid.add(prenUserAjout,1,36);
            grid.add(labnomUserAjout,0,37);
            grid.add(nomUserAjout,1,37);
            grid.add(labmailUserAjout,0,38);
            grid.add(mailUserAjout,1,38);




            Label labprenUserMod = new Label("Prénom : ");
            TextField prenUserMod = new TextField();
            Label labnomUserMod = new Label("Nom : ");
            TextField nomUserMod = new TextField();
            Label labmailUserMod = new Label("Année de naissance : ");
            TextField mailUserMod = new TextField();


            grid.add(labprenUserMod, 10,36);
            grid.add(prenUserMod,11,36);
            grid.add(labnomUserMod,10,37);
            grid.add(nomUserMod,11,37);
            grid.add(labmailUserMod,10,38);
            grid.add(mailUserMod,11,38);


            prenUserAjout.setDisable(true);
            nomUserAjout.setDisable(true);
            mailUserAjout.setDisable(true);

            prenUserMod.setDisable(true);
            nomUserMod.setDisable(true);
            mailUserMod.setDisable(true);

            ComboBox chk = new ComboBox();
            ArrayList<Auteur> users = retrieve.RetrieveAuteur();
            ObservableList<Auteur> usersObs = FXCollections.observableArrayList(users);
            ArrayList<Auteur> users2 = retrieve.RetrieveAuteur();
            ObservableList<Auteur> usersObs2 = FXCollections.observableArrayList(users);

            ComboBox chkSup = new ComboBox();
            chkSup.setItems(usersObs2);

            Label labSup = new Label("Auteur : ");
            grid.add(labSup,20,36);
            grid.add(chkSup,21,36);

            Label labUseramodif = new Label("Auteur : ");
            grid.add(labUseramodif,10,35);
            grid.add(chk, 11,35);
            chk.setDisable(true);

            chk.setItems(usersObs);
            chk.setOnAction(e ->{
                prenUserMod.setText(((Auteur)chk.getValue()).instanceTotale.get(1).get());
                nomUserMod.setText(((Auteur)chk.getValue()).instanceTotale.get(2).get());
                mailUserMod.setText(((Auteur)chk.getValue()).instanceTotale.get(3).get());

            });
            group.selectedToggleProperty().addListener((observable, oldToggle,newToggle) ->{
                if (((RadioButton)newToggle).getText() == "Ajouter") {
                    prenUserAjout.setDisable(false);
                    nomUserAjout.setDisable(false);
                    mailUserAjout.setDisable(false);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    mailUserMod.setDisable(true);
                    chk.setDisable(true);

                    chkSup.setDisable(true);
                }
                else if (((RadioButton)newToggle).getText() == "Modifier") {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    mailUserAjout.setDisable(true);

                    prenUserMod.setDisable(false);
                    nomUserMod.setDisable(false);
                    mailUserMod.setDisable(false);
                    chk.setDisable(false);

                    chkSup.setDisable(true);
                }
                else {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    mailUserAjout.setDisable(true);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    mailUserMod.setDisable(true);
                    chk.setDisable(true);


                    chkSup.setDisable(false);
                }

            });

            Button validation = new Button("Valider");
            Text trigger = new Text();
            grid.add(trigger,11, 41);
            grid.add(validation, 11, 45);
            validation.setOnAction(m -> {
                if (((RadioButton)group.getSelectedToggle()).getText() == "Ajouter"){

                    trigger.setFill(Color.GREEN);
                    trigger.setText("Auteur ajouté");

                    ArrayList<SimpleStringProperty> newInstance = new ArrayList<>();
                    newInstance.add(new SimpleStringProperty(prenUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(nomUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(mailUserAjout.getText()));

                    Auteur newuser = new Auteur(retrieve.conn, newInstance);
                    try {
                        newuser.Insertdb();
                    }
                    catch(SQLException e) {
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Modifier"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Auteur modifié");

                    ArrayList<Object> newdata = new ArrayList<>();
                    newdata.add(((Auteur)chk.getValue()).instanceTotale.get(0).get());
                    newdata.add(prenUserMod.getText());
                    newdata.add(nomUserMod.getText());
                    newdata.add(mailUserMod.getText());
                    try {
                        ((Auteur)chk.getValue()).Updatedb(newdata);
                    }
                    catch(SQLException e){
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Supprimer"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Auteur supprimé");
                    try {

                        ((Auteur) chkSup.getValue()).Deletedb();
                    }
                    catch(SQLException e) {

                    }
                }
                else {
                    trigger.setFill(Color.FIREBRICK);
                    trigger.setText("Veuillez choisir une modification à effectuer");
                }
            });



            VBox rootpagegestionAuteur = new VBox();
            rootpagegestionAuteur.getChildren().addAll(menubar,grid);
            Scene scene = new Scene(rootpagegestionAuteur, 1100,480);
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageLivresGestion(Stage stage, Retrieve retrieve, MenuBar menubar){
            GridPane grid = new GridPane();
            grid.setHgap(5);
            grid.setVgap(5);
            grid.setPadding(new Insets(25, 25, 25, 25));

            RadioButton ajouter = new RadioButton("Ajouter");
            RadioButton modifier = new RadioButton("Modifier");
            RadioButton supprimer = new RadioButton("Supprimer");

            ToggleGroup group = new ToggleGroup();
            ajouter.setToggleGroup(group);
            modifier.setToggleGroup(group);
            supprimer.setToggleGroup(group);

            Label hautpage = new Label("Gestion des Oeuvres");
            hautpage.setFont(Font.font("Tahoma",FontWeight.BOLD,20));
            grid.add(hautpage, 10,0,10,10);
            grid.add(ajouter,0,30);
            grid.add(modifier,10,30);
            grid.add(supprimer,20,30);

            Label labprenUserAjout = new Label("Titre: ");
            TextField prenUserAjout = new TextField();
            Label labnomUserAjout = new Label("Genre : ");
            TextField nomUserAjout = new TextField();




            grid.add(labprenUserAjout,0,37);
            grid.add(prenUserAjout,1,37);
            grid.add(labnomUserAjout,0,38);
            grid.add(nomUserAjout,1,38);




            Label labprenUserMod = new Label("Titre : ");
            TextField prenUserMod = new TextField();
            Label labnomUserMod = new Label("Genre : ");
            TextField nomUserMod = new TextField();


            grid.add(labprenUserMod, 10,37);
            grid.add(prenUserMod,11,37);
            grid.add(labnomUserMod,10,38);
            grid.add(nomUserMod,11,38);


            prenUserAjout.setDisable(true);
            nomUserAjout.setDisable(true);


            prenUserMod.setDisable(true);
            nomUserMod.setDisable(true);


            ComboBox chk = new ComboBox();
            ArrayList<Oeuvre> users = retrieve.RetrieveOeuvre();
            ObservableList<Oeuvre> usersObs = FXCollections.observableArrayList(users);
            ArrayList<Oeuvre> users2 = retrieve.RetrieveOeuvre();
            ObservableList<Oeuvre> usersObs2 = FXCollections.observableArrayList(users);

            ComboBox chkSup = new ComboBox();
            chkSup.setItems(usersObs2);

            Label labSup = new Label("Oeuvre : ");
            grid.add(labSup,20,36);
            grid.add(chkSup,21,36);

            Label labUseramodif = new Label("Oeuvre : ");
            grid.add(labUseramodif,10,35);
            grid.add(chk, 11,35);
            chk.setDisable(true);

            ArrayList<Auteur> auteursenarray = retrieve.RetrieveAuteur();
            ObservableList<Auteur> auteursenObs = FXCollections.observableArrayList(auteursenarray);
            ArrayList<Auteur> auteursenarray2 = retrieve.RetrieveAuteur();
            ObservableList<Auteur> auteursenObs2 = FXCollections.observableArrayList(auteursenarray2);
            ComboBox auteurderoul = new ComboBox();
            ComboBox auteurderoul2 = new ComboBox();

            auteurderoul.setItems(auteursenObs);
            auteurderoul2.setItems(auteursenObs2);
            auteurderoul.setDisable(true);
            auteurderoul2.setDisable(true);

            group.selectedToggleProperty().addListener((observable, oldToggle,newToggle) ->{
                if (((RadioButton)newToggle).getText() == "Ajouter") {
                    prenUserAjout.setDisable(false);
                    nomUserAjout.setDisable(false);
                    auteurderoul.setDisable(false);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    auteurderoul2.setDisable(true);
                    chk.setDisable(true);

                    chkSup.setDisable(true);
                }
                else if (((RadioButton)newToggle).getText() == "Modifier") {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    auteurderoul.setDisable(true);

                    prenUserMod.setDisable(false);
                    nomUserMod.setDisable(false);
                    auteurderoul2.setDisable(false);
                    chk.setDisable(false);

                    chkSup.setDisable(true);
                }
                else {
                    prenUserAjout.setDisable(true);
                    nomUserAjout.setDisable(true);
                    auteurderoul.setDisable(true);

                    prenUserMod.setDisable(true);
                    nomUserMod.setDisable(true);
                    auteurderoul2.setDisable(true);
                    chk.setDisable(true);

                    chkSup.setDisable(false);
                }

            });

            Button validation = new Button("Valider");
            Text trigger = new Text();
            grid.add(trigger,11, 41);
            grid.add(validation, 11, 45);


            Label labder1 = new Label("Auteur : ");
            Label labder2 = new Label("Auteur : ");

            grid.add(labder1,0,36);
            grid.add(labder2,10,36);
            grid.add(auteurderoul,1,36);
            grid.add(auteurderoul2,11,36);

            chk.setItems(usersObs);
            chk.setOnAction(e ->{
                prenUserMod.setText(((Oeuvre)chk.getValue()).instanceTotale.get(1).get());
                nomUserMod.setText(((Oeuvre)chk.getValue()).instanceTotale.get(2).get());
                for (int i = 0; i< auteursenarray.size();i++){
                    if (auteursenarray.get(i).instanceTotale.get(0).get().equals(((Oeuvre)chk.getValue()).instanceTotale.get(3).get())){
                        auteurderoul2.getSelectionModel().select(i);
                    }
                }

            });


            validation.setOnAction(m -> {
                if (((RadioButton)group.getSelectedToggle()).getText() == "Ajouter"){

                    trigger.setFill(Color.GREEN);
                    trigger.setText("Oeuvre ajouté");

                    ArrayList<SimpleStringProperty> newInstance = new ArrayList<>();
                    newInstance.add(new SimpleStringProperty(prenUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(nomUserAjout.getText()));
                    newInstance.add(new SimpleStringProperty(((Auteur)auteurderoul.getValue()).instanceTotale.get(0).get()));
                    Oeuvre newuser = new Oeuvre(retrieve.conn, newInstance);
                    try {
                        newuser.Insertdb();
                    }
                    catch(SQLException e) {
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Modifier"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Oeuvre modifié");

                    ArrayList<Object> newdata = new ArrayList<>();
                    newdata.add(((Oeuvre)chk.getValue()).instanceTotale.get(0).get());
                    newdata.add(prenUserMod.getText());
                    newdata.add(nomUserMod.getText());
                    newdata.add(((Auteur)auteurderoul2.getValue()).instanceTotale.get(0).get());
                    try {
                        ((Oeuvre)chk.getValue()).Updatedb(newdata);
                    }
                    catch(SQLException e){
                        System.out.println(e);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Supprimer"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Oeuvre supprimé");
                    try {

                        ((Oeuvre) chkSup.getValue()).Deletedb();
                    }
                    catch(SQLException e) {

                    }
                }
                else {
                    trigger.setFill(Color.FIREBRICK);
                    trigger.setText("Veuillez choisir une modification à effectuer");
                }
            });



            VBox rootpagegestionOeuvre = new VBox();
            rootpagegestionOeuvre.getChildren().addAll(menubar,grid);
            Scene scene = new Scene(rootpagegestionOeuvre, 1000,480);
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
        public static int pageEmprunts(Stage stage, Retrieve retrieve, MenuBar menubar){
            GridPane grid2 = new GridPane();
            grid2.setHgap(5);
            grid2.setVgap(5);
            grid2.setPadding(new Insets(25, 25, 25, 25));
            Label hautpage = new Label("Emprunter un livre");
            hautpage.setFont(Font.font("Tahoma",FontWeight.BOLD,20));
            grid2.add(hautpage, 10,0,10,10);

            ToggleGroup group = new ToggleGroup();

            RadioButton emprunter = new RadioButton("Emprunter");
            RadioButton rendre = new RadioButton("Rendre");
            emprunter.setToggleGroup(group);
            rendre.setToggleGroup(group);
            grid2.add(emprunter,0,30);
            grid2.add(rendre,25,30);

            ArrayList<OeuvreEmprunt> oeuvresemprunte = retrieve.RetrieveOeuvreEmprunte(retrieve.id_user);
            ObservableList<OeuvreEmprunt> oeuvresemprunteObs = FXCollections.observableArrayList(oeuvresemprunte);


            ComboBox rendable = new ComboBox();
            rendable.setItems(oeuvresemprunteObs);


            ArrayList<Oeuvre> oeuvresempruntable = retrieve.RetrieveOeuvreEmpruntable();
            ObservableList<Oeuvre> oeuvresempruntableObs = FXCollections.observableArrayList(oeuvresempruntable);


            ComboBox empruntable = new ComboBox();
            empruntable.setItems(oeuvresempruntableObs);

            empruntable.setDisable(true);
            rendable.setDisable(true);

            Label labemprunter = new Label("Emprunter : ");
            grid2.add(empruntable, 1,36);
            grid2.add(labemprunter,0,36);

            Label labrendre = new Label("Rendre : ");
            grid2.add(rendable,26,36);
            grid2.add(labrendre,25,36);

            Button validation = new Button("Valider");
            grid2.add(validation,13,40);
            Text trigger = new Text();
            grid2.add(trigger,14,40);

            group.selectedToggleProperty().addListener((observable,oldToggle,newToggle) ->{
                if ((((RadioButton)newToggle).getText() == "Emprunter")) {
                    empruntable.setDisable(false);
                    rendable.setDisable(true);
                }
                else {
                    empruntable.setDisable(true);
                    rendable.setDisable(false);
                }
            });

            validation.setOnAction( e->{
                if (((RadioButton)group.getSelectedToggle()).getText() == "Rendre"){
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Livre rendu !");
                    ArrayList<SimpleStringProperty> newinstance = new ArrayList<>();
                    newinstance.add(((OeuvreEmprunt)rendable.getValue()).instanceTotale.get(0));
                    Emprunt newemprunt = new Emprunt(retrieve.conn, newinstance);
                    try {
                        newemprunt.Deletedb();
                    }
                    catch (SQLException s){
                        System.out.println(s);
                    }
                }
                else if (((RadioButton)group.getSelectedToggle()).getText() == "Emprunter") {
                    trigger.setFill(Color.GREEN);
                    trigger.setText("Livre emprunté !");
                    ArrayList<SimpleStringProperty> newinstance = new ArrayList<>();
                    newinstance.add(((Oeuvre)empruntable.getValue()).instanceTotale.get(0));
                    newinstance.add(new SimpleStringProperty(retrieve.id_user));

                    Emprunt newemprun = new Emprunt(retrieve.conn, newinstance);
                    try {
                        newemprun.Insertdb();
                    }
                    catch (SQLException s) {
                        System.out.println(s);
                    }

                }
            });

            VBox rootpageafficheEmprunt = new VBox();
            rootpageafficheEmprunt.getChildren().addAll(menubar,grid2);
            Scene scene = new Scene(rootpageafficheEmprunt, 1000,480);
            stage.setScene(scene);
            stage.centerOnScreen();



            return 0;
        }
        public static int pageListeEmprunts(Stage stage, Retrieve retrieve, MenuBar menubar){
            ArrayList<Oeuvre> oeuvres = retrieve.RetrieveOeuvreEmprunteavecAuteur(retrieve.id_user);
            ObservableList<Oeuvre> oeuvresObs = FXCollections.observableArrayList(oeuvres);
            VBox rootpoeuv = new VBox();
            TableView<Oeuvre> oeuvresTables = new TableView<>();

            TableColumn<Oeuvre, String> OeuvTitreColumn = new TableColumn<>("Titre");
            OeuvTitreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));

            TableColumn<Oeuvre, String> OeuvGenreColumn = new TableColumn<>("Genre");
            OeuvGenreColumn.setCellValueFactory(new PropertyValueFactory<>("genre"));

            TableColumn<Oeuvre, String> OeuvNomAutCol = new TableColumn<>("Auteur");
            OeuvNomAutCol.setCellValueFactory(new PropertyValueFactory<>("nomauteur"));

            oeuvresTables.getColumns().addAll(OeuvTitreColumn,OeuvNomAutCol,OeuvGenreColumn);
            oeuvresTables.setItems(oeuvresObs);
            rootpoeuv.getChildren().addAll(menubar,oeuvresTables);
            Scene scene = new Scene(rootpoeuv,600,400);
            stage.setTitle("app essai");
            stage.setScene(scene);
            stage.centerOnScreen();
            return 0;
        }
    public void start(Stage stage) {
        Connect con = new Connect();
        con.connect();
        this.controller = new Controller(con);
        this.retrieve = new Retrieve(con);


        VBox root = new VBox();
        MenuBar menubar = new MenuBar();
        Auteurs = new Menu("Auteurs");
        Livres = new Menu("Livres");
        Menu profil = new Menu("Profil");
        MenuItem compte = new MenuItem("Compte");
        MenuItem logout = new MenuItem("Se déconnecter");
        MenuItem param = new MenuItem("Paramètres");
        profil.getItems().addAll(compte,param,logout);

        MenuItem auteursAffiche = new MenuItem("Afficher");
        MenuItem livresAffiche = new MenuItem("Afficher");

        Auteurs.getItems().addAll(auteursAffiche);
        Livres.getItems().addAll(livresAffiche);

        Menu Emprunts = new Menu("Emprunts");
        MenuItem emprunter = new MenuItem("Emprunter");
        MenuItem mesemprunts = new MenuItem("Mes emprunts");
        Emprunts.getItems().addAll(emprunter,mesemprunts);

        menubar.getMenus().addAll(Auteurs,Livres,Emprunts,profil);


        Auteurs.setOnAction(e -> {
            pageAuteur(stage,retrieve,menubar);
        });
        Livres.setOnAction(e -> {
            pageOeuvre(stage, retrieve, menubar);
        });
        logout.setOnAction(e ->{
            pagelogin(stage,con,retrieve,menubar);
        });
        emprunter.setOnAction(e->{
            pageEmprunts(stage,retrieve,menubar);
        });
        mesemprunts.setOnAction(e->{
            pageListeEmprunts(stage,retrieve,menubar);
        });
        stage.show();
        int i = pagelogiciel(stage);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2.1), e ->{pagelogin(stage,con, retrieve, menubar);}));
        timeline.play();
    }
    public static void main(String[] args) {
        launch(args);
    }

}
