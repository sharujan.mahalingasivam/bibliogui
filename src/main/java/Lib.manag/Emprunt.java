package Lib.manag;

import javafx.beans.property.SimpleStringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Emprunt extends Table{
    Emprunt(Connect conn, ResultSet resultatInstance) throws SQLException {
        super(conn, resultatInstance);
    }
    Emprunt(Connect conn, ArrayList<SimpleStringProperty> instanceACreer){
        super(conn, instanceACreer);
        this.nomColonnes.addAll(Arrays.asList("id_emprunt", "id_oeuvre", "id_usager"));
    }



}
