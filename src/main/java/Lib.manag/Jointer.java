package Lib.manag;

import javafx.beans.property.SimpleObjectProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Jointer{
    ArrayList<Oeuvre> arraytable;
    Retrieve retrieve;
    Jointer(ArrayList<Oeuvre> arraytable) {
        this.arraytable = arraytable;
        retrieve = new Retrieve(arraytable.get(0).conn);
    }

    public ArrayList<Oeuvre> tableavecauteursdisplay(){
        ArrayList<Oeuvre> tableavecauteurs = new ArrayList<>();
        int index_idauteur = 0;
        for(int i = 0; i<arraytable.get(0).nomColonnes.size();i++){
            System.out.println(arraytable.get(0).nomColonnes);
            if (arraytable.get(0).nomColonnes.get(i) == "id_auteur"){
                index_idauteur = i;
            }
        }
        index_idauteur = 3;
        ArrayList<Auteur> auteurs = retrieve.RetrieveAuteur();
        for (int j = 0; j<arraytable.size();j++){
            for (int i=0; i< auteurs.size();i++){

                if (arraytable.get(j).instanceTotale.get(index_idauteur).get().equals(auteurs.get(i).instanceTotale.get(0).get())){
                    Oeuvre tmp = arraytable.get(j);
                    tmp.instanceTotale.add(auteurs.get(i).instanceTotale.get(1));
                    tmp.instanceTotale.add(auteurs.get(i).instanceTotale.get(2));
                    tableavecauteurs.add(tmp);
                }
            }
        }
        return tableavecauteurs;

    }

    public boolean isbanned(Utilisateur user){
        ArrayList<Blacklist> blacklist = retrieve.RetrieveBlacklist();
        boolean banned = false;
        for (int j = 0;j<blacklist.size();j++) {
            if (blacklist.get(j).instanceTotale.get(1).get().equals(user.instanceTotale.get(0).get())) {
                banned = true;
            }
        }
        return banned;
    }
}
