package Lib.manag;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleStringProperty;
import org.controlsfx.control.PropertySheet;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Auteur extends Table{

    Auteur(Connect conn, ResultSet resultatInstance) throws SQLException {
        super(conn, resultatInstance);

    }
    Auteur(Connect conn, ArrayList<SimpleStringProperty> instanceACreer){
        super(conn, instanceACreer);
        this.nomColonnes.addAll(Arrays.asList("id_auteur", "prénom","nom","annee_naissance"));
    }

    public String getId_auteur(){
        return instanceTotale.get(0).get();
    }
    public void setId_auteur(String id_auteur){
        SimpleStringProperty tempObj = instanceTotale.get(0);
        tempObj.set(id_auteur);
        this.instanceTotale.set(0, tempObj);
    }
    public String getPrenom(){
        return instanceTotale.get(1).get();
    }
    public void setPrenom(String prenom){
        SimpleStringProperty tempObj = instanceTotale.get(1);
        tempObj.set(prenom);
        this.instanceTotale.set(1, tempObj);
    }
    public String getNom(){
        return instanceTotale.get(2).get();
    }
    public void setNom(String nom){
        SimpleStringProperty tempObj = instanceTotale.get(2);
        tempObj.set(nom);
        this.instanceTotale.set(2,tempObj);
    }
    public String getDate(){
        return instanceTotale.get(3).get();
    }
    public void setDate(String date){
        SimpleStringProperty tempObj = instanceTotale.get(3);
        tempObj.set(date);
        this.instanceTotale.set(3, tempObj);
    }

    @Override
    public String toString() {
        return ""+instanceTotale.get(1).get()+" "+instanceTotale.get(2).get();
    }
}

