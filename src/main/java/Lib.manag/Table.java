package Lib.manag;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.SingleSelectionModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Table {
    String nomTable = this.getClass().getSimpleName();
    Connect conn;
    int numbAttributesInstance;
    ResultSet resultatInstance;
    ResultSetMetaData resultatInstanceRsmd;
    ArrayList<SimpleStringProperty> instanceTotale = new ArrayList<SimpleStringProperty>();
    ArrayList<String> nomColonnes = new ArrayList<String>();
    ArrayList<String> typeColonnes = new ArrayList<String>();


    Table(Connect conn, ArrayList<SimpleStringProperty> InstanceACreer)
    {
        this.conn = conn;
        this.instanceTotale = InstanceACreer;
    }

    Table(Connect conn, ResultSet resultatInstance) throws SQLException {
        this.conn = conn;
        this.resultatInstance = resultatInstance;
        resultatInstanceRsmd = resultatInstance.getMetaData();


        int numbAttributesInstance = resultatInstanceRsmd.getColumnCount() ;
        for (int i = 1; i<= numbAttributesInstance; i++) {


            nomColonnes.add(resultatInstanceRsmd.getColumnName(i));
            typeColonnes.add(resultatInstanceRsmd.getColumnTypeName(i));
            instanceTotale.add(new SimpleStringProperty(resultatInstance.getString(i)));
        }

    }

    void Insertdb() throws SQLException {
            String valeursInserer = "INSERT INTO " + nomTable+"(";
            for (int j =1;j<nomColonnes.size();j++){
                valeursInserer+= nomColonnes.get(j);
                if (j<nomColonnes.size()-1){
                    valeursInserer+=",";
                }
            }
            valeursInserer+=")";
            valeursInserer+=" VALUES (";
            for (int i = 0; i<instanceTotale.size();i++) {
                valeursInserer += "?";
                if (i < instanceTotale.size()-1) {
                    valeursInserer +=",";
                }
                }
            valeursInserer += ");";
            System.out.println(valeursInserer);
            PreparedStatement insertion = conn.con.prepareStatement(valeursInserer);
            for (int i = 0; i<instanceTotale.size();i++) {
                insertion.setString(i+1, instanceTotale.get(i).get());
            }
            insertion.executeUpdate();
            insertion.close();
            conn.con.commit();
            }


    void Updatedb(ArrayList<Object> newdata) throws SQLException {
        String valeursUpdate = "UPDATE " + nomTable +" SET ";
        for (int i = 1; i<instanceTotale.size();i++) {
            valeursUpdate+= nomColonnes.get(i) +" = ? ";
            if(i<instanceTotale.size()-1) {
                valeursUpdate +=",";
            }
        }
        valeursUpdate += " WHERE "+nomColonnes.get(0)+ " = ? ";
        System.out.println(valeursUpdate);
        PreparedStatement ps = conn.con.prepareStatement(valeursUpdate);
        for (int i = 1; i< instanceTotale.size()+1;i++) {
            if (i == instanceTotale.size()) {
                ps.setObject(i,newdata.get(0));
                System.out.println(newdata.get(0));
            }
            else {
                ps.setObject(i,newdata.get(i));
                System.out.println(newdata.get(i));
            }
        }
        ps.executeUpdate();
        ps.close();
        conn.con.commit();
    }

    void Deletedb() throws SQLException{
        String valeursDelete = "DELETE FROM " +nomTable+ " WHERE " + nomColonnes.get(0)+"= ?";
        System.out.println(valeursDelete);
        PreparedStatement ps = conn.con.prepareStatement(valeursDelete);
        System.out.println(valeursDelete);
        ps.setObject(1,instanceTotale.get(0).get());
        ps.executeUpdate();
        ps.close();
        conn.con.commit();
    }


    public ArrayList<SimpleStringProperty> getInstanceTotale() {
        return instanceTotale;
    }


    @Override
    public String toString() {
        ArrayList<Object> instanceTotaleStr = new ArrayList<Object>();
        for (int i = 0; i<instanceTotale.size(); i++) {
            instanceTotaleStr.add(instanceTotale.get(i).get());

        }
        return ""+ instanceTotaleStr;
    }
}





