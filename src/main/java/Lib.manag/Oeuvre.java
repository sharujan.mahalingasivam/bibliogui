package Lib.manag;
import javafx.beans.property.SimpleStringProperty;
import org.controlsfx.control.PropertySheet;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Oeuvre extends Table{

    Oeuvre(Connect conn, ResultSet resultatInstance) throws SQLException {
        super(conn, resultatInstance);
    }
    Oeuvre(Connect conn, ArrayList<SimpleStringProperty> instanceACreer){
        super(conn, instanceACreer);
        this.nomColonnes.addAll(Arrays.asList("id_oeuvre", "titre", "genre","id_auteur"));
    }

    public String getId_auteur(){
        return instanceTotale.get(3).get();
    }
    public void setId_auteur(String id_auteur){
        SimpleStringProperty tempObj = instanceTotale.get(3);
        tempObj.set(id_auteur);
        this.instanceTotale.set(3, tempObj);
    }
    public String getId_oeuvre(){
        return instanceTotale.get(0).get();
    }
    public void setId_oeuvre(String id_oeuvre){
        SimpleStringProperty tempObj = instanceTotale.get(0);
        tempObj.set(id_oeuvre);
        this.instanceTotale.set(0, tempObj);
    }
    public String getTitre(){
        return instanceTotale.get(1).get();
    }
    public void setTitre(String titre){
        SimpleStringProperty tempObj = instanceTotale.get(1);
        tempObj.set(titre);
        this.instanceTotale.set(2,tempObj);
    }

    public String getGenre(){
        return instanceTotale.get(2).get();
    }
    public void setGenre(String genre){
        SimpleStringProperty tempObj = instanceTotale.get(2);
        tempObj.set(genre);
        this.instanceTotale.set(2,tempObj);
    }

    public String getNomauteur(){return ""+instanceTotale.get(4).get()+" "+instanceTotale.get(5).get();}

    @Override
    public String toString() {
        return ""+instanceTotale.get(1).get();
    }
}